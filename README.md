# Data_Pipelines_with_Airflow

Building data pipelines with apache airflow. 
This is from Udacity Data Engineering Nanodegree online course



# Data Pipelines
Data pipeline is a series of steps in which data is processed. 
Data pipeline is also called ETL (Extraction, Transformation and Loading of data) or ELT(Extraction, Loading and Transformation of data). Tools like spark and pandas are commonly used to extract, transform and load data in data warehouses or lakes such as Amazon Redshift and Amazon Elastic Map Reduce (EMR).

Data pipelines are usually modeled in Directed  Acyclic Graphs (DAGs) fashion. DAGs are a special subset of graphs in which the edges between nodes have a specific direction, and no cycles exist.


# Apache Airflow

Apache airflow is an open source tool in which data pipelines are modeled as DAGs. Airflow is programmatically used to write, schedule and monitor workflows. DAG operations involve creation of operators.  Operators define the atomic steps of work that make up a DAG.
An instatiated operator is known as a task. There are different types of operators in airflow. 

# Components of Airflow are:
1. Scheduler: It orchestrates the execution of jobs on a trigger or schedule. The Scheduler chooses how to prioritize the running and execution of tasks within the system
2. Work Queue: This is used by the scheduler in most Airflow installations to deliver tasks that need to be run to the Workers.
3. Worker processes: They execute the operations defined in each DAG. In most Airflow installations, workers pull from the work queue when it is ready to process a task. When the worker completes the execution of the task, it will attempt to process more work from the work queue until there is no further work remaining. When work in the queue arrives, the worker will begin to process it.
4. Database: it saves credentials, connections, history, and configuration. The database, often referred to as the metadata database, also stores the state of all tasks in the system. Airflow components interact with the database with the Python ORM, SQLAlchemy.
5. Web Interface: This provides a control dashboard for users and maintainers. 


# Data Validation
"Data Validation is the process of ensuring that data is present, correct & meaningful. Ensuring the quality of your data through automated validation checks is a critical step in building data pipelines at any organization."

# Data Lineage
The data lineage of a dataset describes the discrete steps involved in the creation, movement, and calculation of that dataset.

Why is Data Lineage important?
Instilling Confidence: Being able to describe the data lineage of a particular dataset or analysis will build confidence in data consumers (engineers, analysts, data scientists, etc.) that our data pipeline is creating meaningful results using the correct datasets. If the data lineage is unclear, its less likely that the data consumers will trust or use the data.
Defining Metrics: Another major benefit of surfacing data lineage is that it allows everyone in the organization to agree on the definition of how a particular metric is calculated.
Debugging: Data lineage helps data engineers track down the root of errors when they occur. If each step of the data movement and transformation process is well described, it's easy to find problems when they occur.
In general, data lineage has important implications for a business. Each department or business unit's success is tied to data and to the flow of data between departments. For e.g., sales departments rely on data to make sales forecasts, while at the same time the finance department would need to track sales and revenue. Each of these departments and roles depend on data, and knowing where to find the data. Data flow and data lineage tools enable data engineers and architects to track the flow of this large web of data.




# Instructions on running airflow programs and assessing the Web UI
1.  run /opt/airflow/start.sh command to start the Airflow webserver
2. Wait for the Airflow web server to be ready
3.Access the Airflow UI by clicking on the blue "Access Airflow" button